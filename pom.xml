<?xml version="1.0" encoding="UTF-8"?>
<!--
    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.
    -->
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>io.axual.client.example</groupId>
    <artifactId>axual-client-example-parent</artifactId>
    <version>5.4.5-SNAPSHOT</version>
    <packaging>pom</packaging>

    <organization>
        <name>Axual B.V.</name>
        <url>http://www.axual.com</url>
    </organization>

    <licenses>
        <license>
            <name>Apache 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <properties>
        <apache.avro.version>1.9.1</apache.avro.version>
        <axual.client.version>5.4.4</axual.client.version>

        <junit.version>4.12</junit.version>
        <org.awaitility.version>2.0.0</org.awaitility.version>
        <slf4j.version>1.7.25</slf4j.version>

        <java.source.version>1.8</java.source.version>
        <java.target.version>1.8</java.target.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <maven.versions.plugin.version>2.7</maven.versions.plugin.version>
        <maven.clean.plugin.version>3.1.0</maven.clean.plugin.version>
        <maven.compiler.plugin.version>3.8.0</maven.compiler.plugin.version>
        <maven.jar.plugin.version>3.1.0</maven.jar.plugin.version>
        <maven.source.plugin.version>3.0.1</maven.source.plugin.version>
        <maven.resources.plugin.version>3.1.0</maven.resources.plugin.version>
        <maven.install.plugin.version>3.0.0-M1</maven.install.plugin.version>
        <maven.deploy.plugin.version>3.0.0-M1</maven.deploy.plugin.version>
        <maven.release.plugin.version>2.5.3</maven.release.plugin.version>
        <maven.enforcer.plugin.version>3.0.0-M2</maven.enforcer.plugin.version>
        <maven.surefire.plugin.version>2.20</maven.surefire.plugin.version>
    </properties>

    <modules>
        <module>axual-client-examples</module>
        <module>data-definitions</module>
        <module>example-common</module>
        <module>axual-client-proxy-examples</module>
        <module>example-standalone</module>
        <module>axual-client-stream-examples</module>
    </modules>

    <scm>
        <developerConnection>scm:git:git@gitlab.com:axual/platform/axual-client-example.git
        </developerConnection>
        <connection>scm:git:git@gitlab.com:axual/platform/axual-client-example.git
        </connection>
        <url>ssh://git@gitlab.com:axual/platform/axual-client-example.git</url>
        <tag>HEAD</tag>
    </scm>

    <dependencyManagement>
        <dependencies>
            <!-- modules -->
            <dependency>
                <groupId>io.axual.client.example</groupId>
                <artifactId>example-common</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>io.axual.client.example</groupId>
                <artifactId>apache-avro-schema-example</artifactId>
                <version>${project.version}</version>
            </dependency>

            <!-- Logging -->
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>${slf4j.version}</version>
            </dependency>
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-simple</artifactId>
                <version>${slf4j.version}</version>
            </dependency>

            <!-- Apache Avro -->
            <dependency>
                <groupId>org.apache.avro</groupId>
                <artifactId>avro</artifactId>
                <version>${apache.avro.version}</version>
            </dependency>

            <!-- Axual Client imports -->
            <dependency>
                <groupId>io.axual.client</groupId>
                <artifactId>axual-client-parent</artifactId>
                <version>${axual.client.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.streams</groupId>
                <artifactId>axual-streams</artifactId>
                <version>${axual.client.version}</version>
            </dependency>
            <dependency>
                <groupId>io.axual.streams</groupId>
                <artifactId>axual-streams-proxy</artifactId>
                <version>${axual.client.version}</version>
            </dependency>

            <!-- Test imports -->
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>${junit.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.awaitility</groupId>
                <artifactId>awaitility</artifactId>
                <version>${org.awaitility.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.platform.test</groupId>
                <artifactId>axual-platform-test-core</artifactId>
                <version>${axual.client.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>io.axual.platform.test</groupId>
                <artifactId>axual-platform-test-junit4</artifactId>
                <version>${axual.client.version}</version>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-clean-plugin</artifactId>
                    <version>${maven.clean.plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${maven.compiler.plugin.version}</version>
                    <configuration>
                        <source>${java.source.version}</source>
                        <target>${java.target.version}</target>
                    </configuration>
                </plugin>
                <plugin>
                    <artifactId>maven-jar-plugin</artifactId>
                    <version>${maven.jar.plugin.version}</version>
                    <configuration>
                        <archive>
                            <manifest>
                                <addDefaultImplementationEntries>true
                                </addDefaultImplementationEntries>
                            </manifest>
                            <manifestEntries>
                                <Created-By>${java.vm.name} ${java.version} (${java.vm.vendor})
                                </Created-By>
                                <Implementation-Vendor-ArtifactId>${project.artifactId}
                                </Implementation-Vendor-ArtifactId>
                            </manifestEntries>
                        </archive>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-source-plugin</artifactId>
                    <version>${maven.source.plugin.version}</version>
                    <configuration>
                        <archive>
                            <manifest>
                                <addDefaultImplementationEntries>true
                                </addDefaultImplementationEntries>
                            </manifest>
                            <manifestEntries>
                                <Created-By>${java.vm.name} ${java.version} (${java.vm.vendor})
                                </Created-By>
                                <Implementation-Vendor-ArtifactId>${project.artifactId}
                                </Implementation-Vendor-ArtifactId>
                            </manifestEntries>
                        </archive>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-install-plugin</artifactId>
                    <version>${maven.install.plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>${maven.deploy.plugin.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>${maven.release.plugin.version}</version>
                    <configuration>
                        <tagNameFormat>@{version}</tagNameFormat>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <version>${spring.boot.version}</version>
                    <executions>
                        <execution>
                            <goals>
                                <goal>repackage</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>${maven.resources.plugin.version}</version>
                    <configuration>
                        <nonFilteredFileExtensions>
                            <nonFilteredFileExtension>jks</nonFilteredFileExtension>
                        </nonFilteredFileExtensions>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-enforcer-plugin</artifactId>
                    <version>${maven.enforcer.plugin.version}</version>
                    <executions>
                        <execution>
                            <id>enforce-maven</id>
                            <goals>
                                <goal>enforce</goal>
                            </goals>
                            <phase>compile</phase>
                            <configuration>
                                <rules>
                                    <requireMavenVersion>
                                        <version>3.5</version>
                                    </requireMavenVersion>
                                </rules>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.codehaus.mojo</groupId>
                    <artifactId>versions-maven-plugin</artifactId>
                    <version>${maven.versions.plugin.version}</version>
                    <configuration>
                        <generateBackupPoms>false</generateBackupPoms>
                    </configuration>
                </plugin>

                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>${maven.surefire.plugin.version}</version>
                    <configuration>
                        <argLine>-Dslf4jtest.print.level=WARN</argLine>
                        <classpathDependencyExcludes>
                            org.springframework.boot:spring-boot-starter-logging
                        </classpathDependencyExcludes>
                    </configuration>
                </plugin>

                <plugin>
                    <artifactId>maven-assembly-plugin</artifactId>
                    <version>3.1.1</version>
                    <configuration>
                        <descriptorRefs>
                            <descriptorRef>jar-with-dependencies</descriptorRef>
                        </descriptorRefs>
                    </configuration>
                    <executions>
                        <execution>
                            <id>make-assembly</id>
                            <goals>
                                <goal>single</goal>
                            </goals>
                            <phase>package</phase>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>

        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>versions-maven-plugin</artifactId>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-enforcer-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
