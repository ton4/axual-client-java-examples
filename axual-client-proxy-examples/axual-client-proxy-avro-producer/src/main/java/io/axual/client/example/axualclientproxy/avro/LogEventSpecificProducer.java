//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.axualclientproxy.avro;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Map;
import java.util.concurrent.Future;

import io.axual.client.example.common.Streams;
import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.example.schema.ApplicationLogLevel;
import io.axual.client.proxy.axual.producer.AxualProducer;

public class LogEventSpecificProducer implements AutoCloseable {
    private final Producer<Application, ApplicationLogEvent> producer;
    private final Application application;

    public LogEventSpecificProducer(
            Map<String, Object> kafkaProperties
            , final String applicationName
            , final String applicationVersion
            , final String applicationOwner) {
        this.producer = new AxualProducer<>(kafkaProperties);
        this.application = Application.newBuilder()
                .setName(applicationName)
                .setVersion(applicationVersion)
                .setOwner(applicationOwner)
                .build();
    }

    public Future<RecordMetadata> produce(
            final String applicationName,
            final long timestamp,
            final ApplicationLogLevel logLevel,
            final String logMessage,
            final Map<CharSequence, CharSequence> context) {
        Application key = Application.newBuilder()
                .setName(applicationName)
                .build();
        ApplicationLogEvent value = ApplicationLogEvent.newBuilder()
                .setTimestamp(timestamp)
                .setSource(this.application)
                .setLevel(logLevel)
                .setMessage(logMessage)
                .setContext(context)
                .build();

        ProducerRecord<Application, ApplicationLogEvent> message = new ProducerRecord<>(
                Streams.AVRO_APPLICATIONLOG.getStream()
                , key, value);
        Future<RecordMetadata> result = producer.send(message);
        return result;
    }

    @Override
    public void close() {
        producer.close();
    }
}
