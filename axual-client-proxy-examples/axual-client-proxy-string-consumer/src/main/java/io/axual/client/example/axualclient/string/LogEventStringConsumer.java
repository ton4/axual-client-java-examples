//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.axualclient.string;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

import io.axual.client.proxy.axual.consumer.AxualConsumer;

public class LogEventStringConsumer implements AutoCloseable, Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(LogEventStringConsumer.class);
    private final AxualConsumer consumer;
    private final LinkedList<ConsumerRecord<String, String>> received = new LinkedList<>();
    private final Future<?> runner;
    private final AtomicBoolean continueRunning;

    public LogEventStringConsumer(Map<String, Object> config, String stream) {
        this.consumer = new AxualConsumer(config);
        this.consumer.subscribe(Collections.singleton(stream));
        this.continueRunning = new AtomicBoolean(true);
        this.runner = Executors.newFixedThreadPool(1).submit(this);
    }

    public void processMessage(ConsumerRecord<String, String> msg) {
        LOG.info("Received message on topic {} partition {} offset {} key {} value {}", msg.topic(), msg.partition(), msg.offset(), msg.key(), msg.value());
        received.add(msg);
    }

    public LinkedList<ConsumerRecord<String, String>> getReceived() {
        return received;
    }

    @Override
    public void close() {
        if (!this.consumer.subscription().isEmpty()) {
            LOG.info("Unsubscribing consumer");
            this.consumer.unsubscribe();
        }
        LOG.info("Closing consumer");
        this.consumer.close();
    }

    boolean isConsuming() {
        return !this.runner.isDone();
    }

    @Override
    public void run() {
        while (this.continueRunning.get()) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(500));
            for (ConsumerRecord<String, String> record : records) {
                processMessage(record);
            }
            this.consumer.commitSync();
        }
        LOG.info("End of run loop, unsubscribe and close");
        this.close();
    }
}
