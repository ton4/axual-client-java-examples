//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.axualclient.string;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;

import io.axual.client.AxualClient;
import io.axual.client.config.ConsumerConfig;
import io.axual.client.consumer.Consumer;
import io.axual.client.consumer.ConsumerMessage;
import io.axual.client.consumer.Processor;

public class LogEventStringSpecificConsumer implements Processor<String, String>, AutoCloseable {
    public static final Logger LOG = LoggerFactory.getLogger(LogEventStringSpecificConsumer.class);
    private final Consumer<String, String> consumer;
    private final LinkedList<ConsumerMessage<String, String>> received = new LinkedList<>();

    public LogEventStringSpecificConsumer(
            final AxualClient axualClient
            , final ConsumerConfig<String, String> consumerConfig) {
        this.consumer = axualClient.buildConsumer(consumerConfig, this);
        this.consumer.startConsuming();
    }

    @Override
    public void processMessage(ConsumerMessage<String, String> msg) {
        LOG.info("Received message on topic {} partition {} offset {} key {} value {}", msg.getSystem(), msg.getPartition(), msg.getOffset(), msg.getKey(), msg.getValue());
        received.add(msg);
    }

    public LinkedList<ConsumerMessage<String, String>> getReceived() {
        return received;
    }

    @Override
    public void close() {
        this.consumer.stopConsuming();
    }

    public boolean isConsuming() {
        return this.consumer.isConsuming();
    }
}
