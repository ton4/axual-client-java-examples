//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.axualclient.string;

import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.ProducerConfig;
import io.axual.client.example.common.Applications;
import io.axual.client.example.common.ServerSettings;
import io.axual.client.producer.ProducedMessage;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;

import static io.axual.client.example.schema.ApplicationLogLevel.INFO;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.HEADER_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.LINEAGE_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;
import static java.lang.Thread.sleep;

public class ClientStringProducerApp {
    private static final Logger LOG = LoggerFactory.getLogger(ClientStringProducerApp.class);
    private static final String APPLICATION_ID = Applications.CLIENT_STRING_PRODUCER.getApplicationId();
    private static final String APPLICATION_VERSION = Applications.CLIENT_STRING_PRODUCER.getApplicationVersion();
    private static final String ENDPOINT = ServerSettings.getEndpoint();
    private static final String TENANT = ServerSettings.TENANT;
    private static final String ENVIRONMENT = Applications.CLIENT_STRING_PRODUCER.getEnvironment();

    private static final String KEYSTORE_LOCATION = getResourceFilePath(Applications.CLIENT_STRING_PRODUCER.getKeystoreLocation());
    private static final PasswordConfig KEYSTORE_PASSWORD = new PasswordConfig(Applications.CLIENT_STRING_PRODUCER.getKeystorePassword());
    private static final PasswordConfig KEY_PASSWORD = new PasswordConfig(Applications.CLIENT_STRING_PRODUCER.getKeyPassword());
    private static final String TRUSTSTORE_LOCATION = getResourceFilePath(Applications.CLIENT_STRING_PRODUCER.getKeystoreLocation());
    private static final PasswordConfig TRUSTSTORE_PASSWORD = new PasswordConfig(Applications.CLIENT_STRING_PRODUCER.getTruststorePassword());

    private static final String APPLICATION_NAME = "Axual Client String Producer";
    private static final String APPLICATION_OWNER = "Team Log";

    public static void main(String[] args) throws InterruptedException {
        ClientConfig config = ClientConfig.newBuilder()
                .setApplicationId(APPLICATION_ID)
                .setApplicationVersion(APPLICATION_VERSION)
                .setEndpoint(ENDPOINT)
                .setTenant(TENANT)
                .setEnvironment(ENVIRONMENT)
                .setSslConfig(
                        SslConfig.newBuilder()
                                .setEnableHostnameVerification(false)
                                .setKeystoreLocation(KEYSTORE_LOCATION)
                                .setKeystorePassword(KEYSTORE_PASSWORD)
                                .setKeyPassword(KEY_PASSWORD)
                                .setTruststoreLocation(TRUSTSTORE_LOCATION)
                                .setTruststorePassword(TRUSTSTORE_PASSWORD)
                                .build()
                )
                .build();

        ProducerConfig<String, String> producerConfig =
                ProducerConfig.<String, String>builder()
                        .setKeySerializer(new StringSerializer())
                        .setValueSerializer(new StringSerializer())
                        .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                        .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                        .setMessageBufferWaitTimeout(100)
                        .setBatchSize(1)
                        .setProxyChain(ProxyChain.newBuilder()
                                .append(SWITCHING_PROXY_ID)
                                .append(RESOLVING_PROXY_ID)
                                .append(LINEAGE_PROXY_ID)
                                .append(HEADER_PROXY_ID)
                                .build())
                        .build();

        List<Future<ProducedMessage<String, String>>> futures;

        try (final AxualClient axualClient = new AxualClient(config);
             final LogEventStringProducer producer = new LogEventStringProducer(axualClient, producerConfig, APPLICATION_NAME, APPLICATION_VERSION, APPLICATION_OWNER)) {
            futures = IntStream.range(0, 10)
                    .mapToObj(i -> producer.produce("app_" + i, 1000 + i, INFO, String.format("Message %d", i), Collections.singletonMap("Some key", "Some Value")))
                    .collect(Collectors.toList());

            do {
                futures.removeIf(future -> {
                    if (!future.isDone()) {
                        return false;
                    }

                    try {
                        ProducedMessage<String, String> producedMessage = future.get();
                        LOG.info("Produced message to topic {} partition {} offset {}", producedMessage.getStream(), producedMessage.getPartition(), producedMessage.getOffset());
                    } catch (InterruptedException | ExecutionException e) {
                        LOG.error("Error getting future, produce failed", e);
                    }
                    return true;
                });
                sleep(100);
            } while (!futures.isEmpty());
        }

        LOG.info("Done!");
    }

    private static String getResourceFilePath(final String resource) {
        return ClassLoader.getSystemClassLoader().getResource(resource).getFile();
    }
}
