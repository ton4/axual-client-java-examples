//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.axualclient.avro;

import org.apache.avro.Schema;
import org.apache.commons.lang3.StringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.SpecificAvroConsumerConfig;
import io.axual.client.config.SpecificAvroProducerConfig;
import io.axual.client.consumer.Consumer;
import io.axual.client.consumer.ConsumerMessage;
import io.axual.client.example.common.Streams;
import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.example.schema.ApplicationLogLevel;
import io.axual.client.exception.ConsumeFailedException;
import io.axual.client.producer.ProducedMessage;
import io.axual.common.config.ClientConfig;
import io.axual.platform.test.core.StreamConfig;
import io.axual.platform.test.junit4.SingleClusterPlatformUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertTrue;

public class LogEventSpecificProducerTest {
    private static final Logger LOG = LoggerFactory.getLogger(LogEventSpecificProducerTest.class);

    private static final String STREAM = Streams.AVRO_APPLICATIONLOG.getStream();
    private static final Schema KEY_SCHEMA = Streams.AVRO_APPLICATIONLOG.getKeySchema();
    private static final Schema VALUE_SCHEMA = Streams.AVRO_APPLICATIONLOG.getValueSchema();
    private static final int PARTITIONS = Streams.AVRO_APPLICATIONLOG.getPartitions();
    private static final String APPLICATION_ID = "io.axual.specific.avro.producer.test";
    private static final String APPLICATION_NAME = "Test LogEventSpecificProducer";
    private static final String APPLICATION_VERSION = "0.0.0";
    private static final String APPLICATION_OWNER = "Test";

    private static final String LOG_MESSAGE_APPLICATION_NAME = "Log App";
    private static final String LOG_MESSAGE_FORMAT = "Message %d";
    private static final String LOG_MESSAGE_CONTEXT_ITERATION_KEY = "iteration";

    private class TestingRecord {
        final int iteration;
        final String applicationName;
        final long timestamp;
        final ApplicationLogLevel logLevel;
        final CharSequence logMessage;
        final Map<CharSequence, CharSequence> context;

        public TestingRecord(int iteration, String applicationName, long timestamp, ApplicationLogLevel logLevel, String logMessage, Map<CharSequence, CharSequence> context) {
            this.iteration = iteration;
            this.applicationName = applicationName;
            this.timestamp = timestamp;
            this.logLevel = logLevel;
            this.logMessage = logMessage;
            this.context = context;
        }

        @Override
        public String toString() {
            return "TestRecord:" +
                    "\titeration       : '" + iteration + "'\n" +
                    "\tapplicationName : '" + applicationName + "'\n" +
                    "\ttimestamp       : '" + timestamp + "'\n" +
                    "\tlogLevel        : '" + logLevel + "'\n" +
                    "\tlogMessage      : '" + logMessage + "'\n"
                    ;
        }
    }

    @Rule
    public SingleClusterPlatformUnit platform = new SingleClusterPlatformUnit()
            .addStream(
                    new StreamConfig()
                            .setName(STREAM)
                            .setKeySchema(KEY_SCHEMA)
                            .setValueSchema(VALUE_SCHEMA)
                            .setPartitions(PARTITIONS)
            );

    @Test
    public void produce() {

        // Create the test set
        List<TestingRecord> testingRecords = IntStream.range(0, 10).mapToObj(iteration -> {
            return new TestingRecord(
                    iteration,
                    LOG_MESSAGE_APPLICATION_NAME
                    , 100 + iteration
                    , ApplicationLogLevel.FATAL
                    , String.format(LOG_MESSAGE_FORMAT, iteration), Collections.singletonMap(LOG_MESSAGE_CONTEXT_ITERATION_KEY, Integer.toString(iteration)));
        }).collect(Collectors.toList());

        // Get the clientConfig
        ClientConfig clientConfig = platform.instance().getClientConfig(APPLICATION_ID);

        // Create queue, processors and configs for the verifying consumer
        Queue<ConsumerMessage<Application, ApplicationLogEvent>> queue = new LinkedList<>();
        EnqueueProcessor processor = new EnqueueProcessor("ProducerTest Queue", queue, true);
        SpecificAvroConsumerConfig<Application, ApplicationLogEvent> consumerConfig = SpecificAvroConsumerConfig
                .<Application, ApplicationLogEvent>builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setStream(STREAM)
                .build();

        // Create producer config
        SpecificAvroProducerConfig<Application, ApplicationLogEvent> producerConfig = SpecificAvroProducerConfig
                .<Application, ApplicationLogEvent>builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                .build();

        try (final AxualClient axualClient = new AxualClient(clientConfig);
             final LogEventSpecificProducer producer = new LogEventSpecificProducer(axualClient, producerConfig, APPLICATION_NAME, APPLICATION_VERSION, APPLICATION_OWNER);
             final Consumer<Application, ApplicationLogEvent> consumer = axualClient.buildConsumer(consumerConfig, processor)) {
            Future<ConsumeFailedException> consumerFuture = consumer.startConsuming();
            List<Future<ProducedMessage<Application, ApplicationLogEvent>>> produceFutures =
                    testingRecords.stream().map(record -> producer.produce(
                            record.applicationName,
                            record.timestamp,
                            record.logLevel,
                            record.logMessage,
                            record.context)
                    ).collect(Collectors.toList());


            LOG.info("Waiting for all producer messages to be produced");
            await("Check producer")
                    .atMost(60, TimeUnit.SECONDS)
                    .pollDelay(1, TimeUnit.SECONDS)
                    .pollInterval(500, TimeUnit.MILLISECONDS)
                    .until(() -> {
                        produceFutures.removeIf(future -> {
                            if (future.isDone()) {
                                try {
                                    ProducedMessage<Application, ApplicationLogEvent> message = future.get();
                                    LOG.info("Produced message at {} on \n" +
                                                    "stream    : {}\n" +
                                                    "partition : {}\n" +
                                                    "offset    : {}\n" +
                                                    "cluster   : {}\n" +
                                                    "system    : {}\n" +
                                                    "instance  : {}\n"
                                            , message.getTimestamp()
                                            , message.getStream()
                                            , message.getPartition()
                                            , message.getOffset()
                                            , message.getCluster()
                                            , message.getSystem()
                                            , message.getInstance()
                                    );
                                    return true;
                                } catch (InterruptedException | ExecutionException e) {
                                    e.printStackTrace();
                                }
                            }
                            return false;
                        });
                        return produceFutures.isEmpty();
                    });

            LOG.info("Waiting for all Consumer messages to be read");
            await("Checking nr of consumer messages")
                    .atMost(15, TimeUnit.SECONDS)
                    .pollDelay(1, TimeUnit.SECONDS)
                    .pollInterval(500, TimeUnit.MILLISECONDS)
                    .until(() -> queue.size() >= testingRecords.size());

            final ConsumeFailedException consumeFailedExceptionFromStop = consumer.stopConsuming();
            if (consumeFailedExceptionFromStop != null) {
                LOG.warn("Consume failed from stop", consumeFailedExceptionFromStop);
            }

            await("Wait for consumer to be fully stopped")
                    .atMost(15, TimeUnit.SECONDS)
                    .pollDelay(1, TimeUnit.SECONDS)
                    .pollInterval(500, TimeUnit.MILLISECONDS)
                    .until(new Callable<Boolean>() {
                        @Override
                        public Boolean call() throws Exception {
                            return consumerFuture.isDone();
                        }
                    });
            try {
                if (consumerFuture.isCancelled()) {
                    LOG.warn("Consumer was cancelled");
                } else {
                    final ConsumeFailedException consumeFailedExceptionFromFuture = consumerFuture.get();
                    if (consumeFailedExceptionFromFuture != null) {
                        LOG.warn("Consume failed from future", consumeFailedExceptionFromFuture);
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        /* for each test record
             2 set foundOnQueue to false
             3 while foundOnQueue is false and Queue is not empty
             31 get consumed from queue
             33 foundOnQueue = testRecord Equals ApplicationLogEvent
             4 assertTrue( foundOnQueue )
         */
        for (TestingRecord record : testingRecords) {
            boolean foundOnQueue = false;
            while (!foundOnQueue && !queue.isEmpty()) {
                ConsumerMessage<Application, ApplicationLogEvent> consumed = queue.poll();
                foundOnQueue = checkEquality(record, consumed.getKey(), consumed.getValue());
            }
            assertTrue(String.format("Record not found %s", record), foundOnQueue);
        }
    }

    private boolean checkEquality(final TestingRecord record, final Application application, final ApplicationLogEvent logEvent) {
        if (record == null || logEvent == null) {
            return false;
        }


        boolean match =
                StringUtils.equals(record.applicationName, application.getName())
                        && StringUtils.equals(record.logMessage, logEvent.getMessage())
                        && record.timestamp == logEvent.getTimestamp()
                        && record.logLevel == logEvent.getLevel();
        return match;
    }

}