//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.axualclient.avro;

import org.apache.avro.generic.GenericContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Queue;

import io.axual.client.consumer.ConsumerMessage;
import io.axual.client.consumer.Processor;

public class EnqueueProcessor<K extends GenericContainer, V extends GenericContainer> implements Processor<K, V> {
    private static final Logger LOG = LoggerFactory.getLogger(EnqueueProcessor.class);
    private final String name;
    private final Queue<ConsumerMessage<K, V>> queue;
    private final boolean logOutput;

    public EnqueueProcessor(String name, Queue<ConsumerMessage<K, V>> queue) {
        this(name, queue, false);
    }

    public EnqueueProcessor(String name, Queue<ConsumerMessage<K, V>> queue, boolean logOutput) {
        this.name = name;
        this.queue = queue;
        this.logOutput = logOutput;
    }

    @Override
    public void processMessage(ConsumerMessage<K, V> message) {
        if (logOutput) {
            LOG.info("Queue \"{}\" received message: KEY = {}, MESSAGE = {}", name, message.getKey(), message.getValue());
        }
        queue.add(message);
    }
}

