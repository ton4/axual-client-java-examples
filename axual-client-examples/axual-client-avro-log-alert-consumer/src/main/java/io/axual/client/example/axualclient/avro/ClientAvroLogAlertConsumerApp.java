//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.axualclient.avro;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.SpecificAvroConsumerConfig;
import io.axual.client.example.common.Applications;
import io.axual.client.example.common.ServerSettings;
import io.axual.client.example.common.Streams;
import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogAlert;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;

import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.HEADER_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.LINEAGE_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;
import static java.lang.Thread.sleep;

public class ClientAvroLogAlertConsumerApp {
    private static final String APPLICATION_ID = Applications.CLIENT_AVRO_LOG_ALERT_CONSUMER.getApplicationId();
    private static final String APPLICATION_VERSION = Applications.CLIENT_AVRO_LOG_ALERT_CONSUMER.getApplicationVersion();
    private static final String ENDPOINT = ServerSettings.getEndpoint();
    private static final String TENANT = ServerSettings.TENANT;
    private static final String ENVIRONMENT = Applications.CLIENT_AVRO_LOG_ALERT_CONSUMER.getEnvironment();

    private static final String KEYSTORE_LOCATION = getResourceFilePath(Applications.CLIENT_AVRO_LOG_ALERT_CONSUMER.getKeystoreLocation());
    private static final PasswordConfig KEYSTORE_PASSWORD = new PasswordConfig(Applications.CLIENT_AVRO_LOG_ALERT_CONSUMER.getKeystorePassword());
    private static final PasswordConfig KEY_PASSWORD = new PasswordConfig(Applications.CLIENT_AVRO_LOG_ALERT_CONSUMER.getKeyPassword());
    private static final String TRUSTSTORE_LOCATION = getResourceFilePath(Applications.CLIENT_AVRO_LOG_ALERT_CONSUMER.getKeystoreLocation());
    private static final PasswordConfig TRUSTSTORE_PASSWORD = new PasswordConfig(Applications.CLIENT_AVRO_LOG_ALERT_CONSUMER.getTruststorePassword());

    public static void main(String[] args) throws InterruptedException {
        ClientConfig config = ClientConfig.newBuilder()
                .setApplicationId(APPLICATION_ID)
                .setApplicationVersion(APPLICATION_VERSION)
                .setEndpoint(ENDPOINT)
                .setTenant(TENANT)
                .setEnvironment(ENVIRONMENT)
                .setSslConfig(
                        SslConfig.newBuilder()
                                .setEnableHostnameVerification(false)
                                .setKeystoreLocation(KEYSTORE_LOCATION)
                                .setKeystorePassword(KEYSTORE_PASSWORD)
                                .setKeyPassword(KEY_PASSWORD)
                                .setTruststoreLocation(TRUSTSTORE_LOCATION)
                                .setTruststorePassword(TRUSTSTORE_PASSWORD)
                                .build()
                )
                .build();


        SpecificAvroConsumerConfig<Application, ApplicationLogAlert> specificAvroConsumerConfig =
                SpecificAvroConsumerConfig.<Application, ApplicationLogAlert>builder()
                        .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                        .setStream(Streams.LOG_ALERT.getStream())
                        .setProxyChain(ProxyChain.newBuilder()
                                .append(SWITCHING_PROXY_ID)
                                .append(RESOLVING_PROXY_ID)
                                .append(LINEAGE_PROXY_ID)
                                .append(HEADER_PROXY_ID)
                                .build())
                        .build();

        try (final AxualClient axualClient = new AxualClient(config);
             final LogAlertSpecificConsumer consumer = new LogAlertSpecificConsumer(axualClient, specificAvroConsumerConfig)) {
            while (consumer.isConsuming()) {
                sleep(100);
            }
        }
    }

    private static String getResourceFilePath(final String resource) {
        return ClassLoader.getSystemClassLoader().getResource(resource).getFile();
    }
}
