//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.axualclient.avro;

import java.util.Arrays;
import java.util.concurrent.Future;

import io.axual.client.AxualClient;
import io.axual.client.config.SpecificAvroProducerConfig;
import io.axual.client.example.common.Streams;
import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogAlertSetting;
import io.axual.client.example.schema.ApplicationLogLevel;
import io.axual.client.producer.ProducedMessage;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;


public class LogAlertSettingSpecificProducer implements AutoCloseable {
    private final Producer<Application, ApplicationLogAlertSetting> producer;
    private final Application application;

    public LogAlertSettingSpecificProducer(
            final AxualClient axualClient
            , final SpecificAvroProducerConfig<Application, ApplicationLogAlertSetting> producerConfig
            , final String applicationName
            , final String applicationVersion
            , final String applicationOwner) {
        producer = axualClient.buildProducer(producerConfig);
        this.application = Application.newBuilder()
                .setName(applicationName)
                .setVersion(applicationVersion)
                .setOwner(applicationOwner)
                .build();
    }

    public Future<ProducedMessage<Application, ApplicationLogAlertSetting>> produce(
            final String applicationName,
            final ApplicationLogLevel logLevel) {
        Application key = Application.newBuilder()
                .setName(applicationName)
                .setVersion("1.9.9")
                .setOwner("none")
                .build();
        ApplicationLogAlertSetting value = ApplicationLogAlertSetting.newBuilder()
                .setSource(applicationName)
                .setLogLevels(Arrays.asList(logLevel))
                .setAddresses(Arrays.asList("Amsterdam","Utrecht"))
                .build();

        ProducerMessage<Application, ApplicationLogAlertSetting> message = ProducerMessage.<Application, ApplicationLogAlertSetting>newBuilder()
                .setStream(Streams.LOG_ALERT_SETTING.getStream())
                .setKey(key)
                .setValue(value)
                .build();
        Future<ProducedMessage<Application, ApplicationLogAlertSetting>> result = producer.produce(message);
        return result;
    }

    @Override
    public void close() {
        producer.close();
    }
}
