//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.examples.axualclient.avro;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.axual.client.config.DeliveryStrategy;
import io.axual.client.example.common.Applications;
import io.axual.client.example.common.ServerSettings;
import io.axual.client.example.common.Streams;
import io.axual.client.example.schema.Application;
import io.axual.client.example.schema.ApplicationLogEvent;
import io.axual.client.example.schema.ApplicationLogLevel;
import io.axual.client.proxy.axual.serde.AxualDeserializer;
import io.axual.client.proxy.axual.serde.AxualDeserializerConfig;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.serde.avro.GenericAvroDeserializer;
import io.axual.serde.avro.GenericAvroSerde;
import io.axual.serde.avro.SpecificAvroSerde;
import io.axual.serde.avro.SpecificAvroSerializer;
import io.axual.streams.AxualStreamsClient;
import io.axual.streams.config.StreamRunnerConfig;
import io.axual.streams.proxy.generic.factory.TopologyFactory;
import io.axual.streams.streams.DefaultHandlerFactory;
import io.axual.streams.streams.StreamRunner;

import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.HEADER_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.LINEAGE_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;

public class ClientStreamAvroToStringApp {

    private static final Logger LOG = LoggerFactory.getLogger(ClientStreamAvroToStringApp.class);
    private static final String APPLICATION_ID = Applications.CLIENT_AVRO_STREAM.getApplicationId();
    private static final String APPLICATION_VERSION = Applications.CLIENT_AVRO_STREAM.getApplicationVersion();
    private static final String ENDPOINT = ServerSettings.getEndpoint();
    private static final String TENANT = ServerSettings.TENANT;
    private static final String ENVIRONMENT = Applications.CLIENT_AVRO_STREAM.getEnvironment();

    private static final String KEYSTORE_LOCATION = getResourceFilePath(Applications.CLIENT_AVRO_STREAM.getKeystoreLocation());
    private static final PasswordConfig KEYSTORE_PASSWORD = new PasswordConfig(Applications.CLIENT_AVRO_STREAM.getKeystorePassword());
    private static final PasswordConfig KEY_PASSWORD = new PasswordConfig(Applications.CLIENT_AVRO_STREAM.getKeyPassword());
    private static final String TRUSTSTORE_LOCATION = getResourceFilePath(Applications.CLIENT_AVRO_STREAM.getKeystoreLocation());
    private static final PasswordConfig TRUSTSTORE_PASSWORD = new PasswordConfig(Applications.CLIENT_AVRO_STREAM.getTruststorePassword());

    public static void main(String[] args) {
        LOG.info("Creating ClientConfig...");
        ClientConfig config = ClientConfig.newBuilder()
                .setApplicationId(APPLICATION_ID)
                .setApplicationVersion(APPLICATION_VERSION)
                .setEndpoint(ENDPOINT)
                .setTenant(TENANT)
                .setEnvironment(ENVIRONMENT)
                .setSslConfig(
                        SslConfig.newBuilder()
                                .setEnableHostnameVerification(false)
                                .setKeystoreLocation(KEYSTORE_LOCATION)
                                .setKeystorePassword(KEYSTORE_PASSWORD)
                                .setKeyPassword(KEY_PASSWORD)
                                .setTruststoreLocation(TRUSTSTORE_LOCATION)
                                .setTruststorePassword(TRUSTSTORE_PASSWORD)
                                .build()
                )
                .build();

        LOG.info("Creating Topology...");
        /*
         * Steps:
         * 1- Read stream from avro-applicationlog.
         * 2- Filter the record based on logging level.
         * 3- Transform the record (key and value) from avro to string format.
         * 4- Write to string-applicationlog.
         * */
        final TopologyFactory topology = builder -> {
            KStream<Application, ApplicationLogEvent> sourceStream = builder.stream(Streams.AVRO_APPLICATIONLOG.getStream());
            sourceStream
                    .peek((k, v) -> LOG.info("Key: {} Value: {}", k, v))
                    .filter((k, v) -> v.getLevel().equals(ApplicationLogLevel.INFO))
                    .map((k, v) -> new KeyValue<>(k.toString(), v.toString()))
                    .to(Streams.STRING_APPLICATIONLOG.getStream(), Produced.with(Serdes.String(), Serdes.String()));
            return builder.build();
        };

        LOG.info("Creating StreamRunnerConfig...");
        // Prepare streamRunner
        final StreamRunnerConfig streamRunnerConfig = StreamRunnerConfig.builder()
                .setUncaughtExceptionHandler(new DefaultHandlerFactory())
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setDefaultKeySerde(SpecificAvroSerde.class.getName())
                .setDefaultValueSerde(SpecificAvroSerde.class.getName())
                .setProxyChain(ProxyChain.newBuilder()
                        .append(SWITCHING_PROXY_ID)
                        .append(RESOLVING_PROXY_ID)
                        .append(LINEAGE_PROXY_ID)
                        .append(HEADER_PROXY_ID)
                        .build())
                .setTopologyFactory(topology)
                .build();

        LOG.info("Creating AxualStreamsClient and StreamRunner...");
        AxualStreamsClient streamsClient = new AxualStreamsClient(config);
        StreamRunner streamRunner = streamsClient.buildStreamRunner(streamRunnerConfig);
        streamRunner.start();
    }

    private static String getResourceFilePath(final String resource) {
        return ClassLoader.getSystemClassLoader().getResource(resource).getFile();
    }
}
