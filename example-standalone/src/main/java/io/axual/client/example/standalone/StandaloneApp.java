//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.standalone;


import io.axual.client.example.common.Applications;
import io.axual.client.example.common.ServerSettings;
import io.axual.client.example.common.Streams;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;
import io.axual.platform.test.core.ClusterUnitConfig;
import io.axual.platform.test.core.InstanceUnitConfig;
import io.axual.platform.test.core.PlatformUnit;
import io.axual.platform.test.core.StreamConfig;

import static java.lang.Thread.sleep;

public class StandaloneApp {
    private static String getResourcePath(String resource) {
        return ClassLoader.getSystemClassLoader().getResource(resource).getFile();
    }

    public static void main(String[] args) throws InterruptedException {
        SslConfig sslConfig = SslConfig.newBuilder()
                .setEnableHostnameVerification(false)
                .setKeystoreLocation(getResourcePath(ServerSettings.KEYSTORE_RESOURCE_PATH))
                .setKeystorePassword(new PasswordConfig(ServerSettings.KEYSTORE_PASSWORD))
                .setKeyPassword(new PasswordConfig(ServerSettings.KEY_PASSWORD))
                .setTruststoreLocation(getResourcePath(ServerSettings.TRUSTSTORE_RESOURCE_PATH))
                .setTruststorePassword(new PasswordConfig(ServerSettings.TRUSTSTORE_PASSWORD))
                .build();
        InstanceUnitConfig instanceUnitConfig = new InstanceUnitConfig()
                .setName(ServerSettings.INSTANCE)
                .setSystem(ServerSettings.SYSTEM)
                .setTenant(ServerSettings.TENANT)
                .setDiscoveryBindAddress(ServerSettings.LISTENADDRESS)
                .setDiscoveryAdvertisedAddress(ServerSettings.LISTENADDRESS)
                .setDiscoveryPort(ServerSettings.ENDPOINTPORT)
                .setSslConfig(sslConfig)
                .addCluster(ServerSettings.CLUSTER);

        ClusterUnitConfig clusterUnitConfig = new ClusterUnitConfig()
                .setName(ServerSettings.CLUSTER)
                .setBindAddress(ServerSettings.LISTENADDRESS)
                .setAdvertisedAddress(ServerSettings.LISTENADDRESS)
                .setBrokerPort(ServerSettings.BROKERPORT)
                .setZookeeperPort(ServerSettings.ZOOKEEPERPORT)
                .setSchemaRegistryPort(ServerSettings.SCHEMAREGISTRYPORT)
                .setTopicPattern(ServerSettings.TOPICPATTERN)
                .setGroupPattern(ServerSettings.GROUPPATTERN)
                .setSslConfig(sslConfig);

        try (PlatformUnit platformUnit = new PlatformUnit(instanceUnitConfig, clusterUnitConfig)) {

            platformUnit.start();

            for (Streams stream : Streams.values()) {
                StreamConfig streamConfig = new StreamConfig();
                streamConfig.setName(stream.getStream());
                streamConfig.setEnvironment(ServerSettings.ENVIRONMENT);
                streamConfig.setPartitions(stream.getPartitions());
                if (stream.getKeySchema() != null) {
                    streamConfig.setKeySchema(stream.getKeySchema());
                }
                if (stream.getValueSchema() != null) {
                    streamConfig.setValueSchema(stream.getValueSchema());
                }
                platformUnit.addStream(streamConfig);
            }

            for (Applications application : Applications.values()) {
                ClientConfig clientConfig = ClientConfig.newBuilder()
                        .setTenant(ServerSettings.TENANT)
                        .setApplicationId(application.getApplicationId())
                        .setApplicationVersion(application.getApplicationVersion())
                        .setEnvironment(application.getEnvironment())
                        .setSslConfig(sslConfig)
                        .setEndpoint(platformUnit.getInstance().getDiscoveryUnit().getUrl())
                        .build();
                platformUnit.getInstance().directApplicationTo(clientConfig, platformUnit.getCluster(0));
            }

            while (true) {
                sleep(100);
            }
        }
    }
}
