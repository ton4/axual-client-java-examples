//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

package io.axual.client.example.common;

public enum Applications {
    CLIENT_AVRO_CONSUMER("io.axual.example.client.avro.consumer", "0.0.1", ServerSettings.ENVIRONMENT, "", "axual.client.keystore.jks", "notsecret", "notsecret", "axual.client.truststore.jks", "notsecret"),
    CLIENT_AVRO_PRODUCER("io.axual.example.client.avro.producer", "0.0.1", ServerSettings.ENVIRONMENT, "", "axual.client.keystore.jks", "notsecret", "notsecret", "axual.client.truststore.jks", "notsecret"),
    CLIENT_AVRO_LOG_ALERT_CONSUMER("io.axual.example.client.avro.log.alert.consumer", "0.0.1", ServerSettings.ENVIRONMENT, "", "axual.client.keystore.jks", "notsecret", "notsecret", "axual.client.truststore.jks", "notsecret"),
    CLIENT_AVRO_LOG_ALERT_SETTING_PRODUCER("io.axual.example.client.avro.log.alert.setting.producer", "0.0.1", ServerSettings.ENVIRONMENT, "", "axual.client.keystore.jks", "notsecret", "notsecret", "axual.client.truststore.jks", "notsecret"),
    CLIENT_STRING_CONSUMER("io.axual.example.client.string.consumer", "0.0.1", ServerSettings.ENVIRONMENT, "", "axual.client.keystore.jks", "notsecret", "notsecret", "axual.client.truststore.jks", "notsecret"),
    CLIENT_STRING_PRODUCER("io.axual.example.client.string.producer", "0.0.1", ServerSettings.ENVIRONMENT, "", "axual.client.keystore.jks", "notsecret", "notsecret", "axual.client.truststore.jks", "notsecret"),
    PROXY_AVRO_CONSUMER("io.axual.example.proxy.avro.consumer", "0.0.1", ServerSettings.ENVIRONMENT, "", "axual.client.keystore.jks", "notsecret", "notsecret", "axual.client.truststore.jks", "notsecret"),
    PROXY_AVRO_PRODUCER("io.axual.example.proxy.avro.producer", "0.0.1", ServerSettings.ENVIRONMENT, "", "axual.client.keystore.jks", "notsecret", "notsecret", "axual.client.truststore.jks", "notsecret"),
    PROXY_STRING_CONSUMER("io.axual.example.proxy.string.consumer", "0.0.1", ServerSettings.ENVIRONMENT, "", "axual.client.keystore.jks", "notsecret", "notsecret", "axual.client.truststore.jks", "notsecret"),
    PROXY_STRING_PRODUCER("io.axual.example.proxy.string.producer", "0.0.1", ServerSettings.ENVIRONMENT, "", "axual.client.keystore.jks", "notsecret", "notsecret", "axual.client.truststore.jks", "notsecret"),
    CLIENT_AVRO_STREAM("io.axual.example.client.avro.stream", "0.0.1", ServerSettings.ENVIRONMENT, "", "axual.client.keystore.jks", "notsecret", "notsecret", "axual.client.truststore.jks", "notsecret"),
    CLIENT_AVRO_JOIN_STREAM("io.axual.example.client.avro.stream.join", "0.0.1", ServerSettings.ENVIRONMENT, "store", "axual.client.keystore.jks", "notsecret", "notsecret", "axual.client.truststore.jks", "notsecret");

    private final String applicationId;
    private final String applicationVersion;
    private final String environment;
    private final String store;
    private final String keystoreLocation;
    private final String keystorePassword;
    private final String keyPassword;
    private final String truststoreLocation;
    private final String truststorePassword;

    Applications(String applicationId, String applicationVersion, String environment, String store, String keystoreLocation, String keystorePassword, String keyPassword, String truststoreLocation, String truststorePassword) {
        this.applicationId = applicationId;
        this.applicationVersion = applicationVersion;
        this.environment = environment;
        this.store = store;
        this.keystoreLocation = keystoreLocation;
        this.keystorePassword = keystorePassword;
        this.keyPassword = keyPassword;
        this.truststoreLocation = truststoreLocation;
        this.truststorePassword = truststorePassword;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public String getApplicationVersion() {
        return applicationVersion;
    }

    public String getEnvironment() {
        return environment;
    }

    public String getStore() {
        return store;
    }

    public String getKeystoreLocation() {
        return keystoreLocation;
    }

    public String getKeystorePassword() {
        return keystorePassword;
    }

    public String getKeyPassword() {
        return keyPassword;
    }

    public String getTruststoreLocation() {
        return truststoreLocation;
    }

    public String getTruststorePassword() {
        return truststorePassword;
    }
}
